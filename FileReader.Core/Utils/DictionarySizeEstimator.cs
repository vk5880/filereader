﻿namespace FileReader.Core.Utils
{
    using System;
    using System.Collections.Generic;

    class DictionarySizeEstimator
    {
        public static int FindMaxDictionarySize<TKey, TValue>(int lowbound, int highbound, int increment = 1)
        {
            GC.Collect();

            long lo = lowbound;
            long hi = highbound;
            int lastGoodSize = 0;
            int size;
            do
            {
                size = (hi + lo) / 2 < int.MaxValue ? (int)((hi + lo) / 2) : int.MaxValue;
                try
                {
                    var ht = new Dictionary<TKey, TValue>(size);
                    ht = null;
                    lastGoodSize = size;
                    lo = size + increment;
                }
                catch (OutOfMemoryException)
                {
                    hi = size - increment;
                }
            } while (hi >= lo);

            GC.Collect();

            return lastGoodSize;
        }
    }
}
