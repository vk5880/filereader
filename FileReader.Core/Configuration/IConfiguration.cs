﻿namespace FileReader.Core.Configuration
{
    public interface IConfiguration
    {
        string ReaderName { get; }

        bool ResultCaching { get; }

        int ResultMaxLength { get; }

        int ReadBufferSize { get; }

        int RAMLimitMb { get; }
    }
}
