﻿namespace FileReader.Core.Configuration
{
    using System;

    public class HardCodedConfiguration : IConfiguration
    {
        public int ResultMaxLength
        {
            get
            {
                return 1024;
            }
        }

        public int ReadBufferSize
        {
            get
            {
                return 32 * 1024 * 1024;
            }
        }

        public int RAMLimitMb
        {
	        get 
	        {
                //build for x64 with /LARGEADDRESSAWARE if want more without OutOfMemoryException
                return 768;
	        }
        }

        public string ReaderName { get; set; }

        public bool ResultCaching { get; set;}

        public HardCodedConfiguration()
        {
            ResultCaching = true;
        }
    }
}
