﻿namespace FileReader.Core.Caches
{
	using System;
    using System.Collections.Generic;
    using System.Collections;
    using FileReader.Core.Utils;
    using System.Runtime.InteropServices;

	class DictionaryCache<TKey, TValue> : ICache<TKey, TValue>
	{
        int _capacity;
        int _maxCapacity;

        Dictionary<TKey, TValue> _cache;

		public DictionaryCache(int capacity, int memoryLimitMb)
		{
            var idealDictionaryCapacity = int.MaxValue / (Marshal.SizeOf(typeof(TKey)) + Marshal.SizeOf(typeof(TValue)));
            var maxMemoryCapacity = memoryLimitMb * 1024 * 1024 / (Marshal.SizeOf(typeof(TKey)) + Marshal.SizeOf(typeof(TValue)));
            var maxRequestedCapacity = Math.Min(capacity, Math.Min(idealDictionaryCapacity, maxMemoryCapacity));
            _maxCapacity = DictionarySizeEstimator.FindMaxDictionarySize<TKey, TValue>(maxRequestedCapacity / 10, maxRequestedCapacity);
            _cache = new Dictionary<TKey, TValue>(_maxCapacity);
            _capacity = 0;
		}

        public TValue this[TKey key]
        {
            get 
            {
                var value = _cache[key];

                return value;
            }
            set
            {
                if (_capacity < _maxCapacity)
                {
                    _cache[key] = value;
                    _capacity++;
                }
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _cache.TryGetValue(key, out value);
        }

        public int Size { get { return _capacity; } }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _cache.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _cache.GetEnumerator();
        }
    }
}
