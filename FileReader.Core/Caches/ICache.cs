﻿namespace FileReader.Core.Caches
{
    using System.Collections.Generic;

    interface ICache<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        TValue this[TKey key] { get; set; }
        bool TryGetValue(TKey key, out TValue value);
        int Size { get; }
    }
}
