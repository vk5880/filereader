﻿namespace FileReader.Core
{
	using System;
	using System.Collections.Specialized;
	using System.Collections.Generic;
	using SRC=System.Runtime.Caching;
	using FileReader.Core.Configuration;

	sealed class MemoryCache : IDisposable
	{
		static MemoryCache _instance;
		SRC.MemoryCache _cache;

		public static void Initialize(int memoryLimitInMb)
		{
			if (_instance == null)
				_instance = new MemoryCache(memoryLimitInMb);
			else
				throw new InvalidOperationException("Cache has been initialized already.");
		}

		public static bool Initialized
		{
			get
			{
				return _instance != null;
			}
		}
		
		public static MemoryCache Instance
		{
			get
			{
				if (_instance == null) throw new InvalidOperationException("Cache has not been initialized. Call Initialize() first.");

				return _instance;
			}
		}

		public static void Free()
		{
			if (_instance != null)
			{
				_instance.Dispose();
				_instance = null;
			}
		}

		private MemoryCache(int memoryLimitInMb)
		{
			var cacheConfig = new NameValueCollection();
			cacheConfig.Add("CacheMemoryLimitMegabytes", memoryLimitInMb.ToString());
			_cache = new SRC.MemoryCache("FileReaderCache", cacheConfig);
		}

		public string GetLine(long lineIndex)
		{
			CheckCacheState();
			
			var lineKey = GetLineCacheKey(lineIndex);
			var line = _cache[lineKey] as string;
			
			return line;
		}
		
		public void CacheLine(long lineIndex, string line)
		{
			CheckCacheState();
			
			if(line == null) return;
			
			var lineKey = GetLineCacheKey(lineIndex);
			_cache[lineKey] = line;
		}

		public long CacheSize()
		{
			CheckCacheState();
			
			return _cache.GetCount();
		}

		public string GetWord(long lineIndex, long wordIndex)
		{
			CheckCacheState();
			
			var wordKey = GetWordCacheKey(lineIndex, wordIndex);
			var word = _cache[wordKey] as string;
			
			return word;
		}
		
		public void CacheWord(long lineIndex, long wordIndex, string word)
		{
			CheckCacheState();
				
			if(word == null) return;
			
			var wordKey = GetWordCacheKey(lineIndex, wordIndex);
			_cache[wordKey] = word;
		}
		
		public void Dispose()
		{
			if (_cache != null)
			{
				_cache.Dispose();
				_cache = null;
			}
		}
		
		private void CheckCacheState()
		{
			if (_cache == null) throw new ObjectDisposedException("cache");
		}
		
		private string GetLineCacheKey(long lineIndex)
		{
			return lineIndex.ToString();
		}

		private string GetWordCacheKey(long lineIndex, long wordIndex)
		{
			return string.Format("{0}_{1}", lineIndex, wordIndex);
		}
	}
}
