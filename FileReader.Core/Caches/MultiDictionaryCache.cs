﻿namespace FileReader.Core.Caches
{
	using System;
    using System.Collections.Generic;
    using System.Collections;
    using FileReader.Core.Utils;
    using System.Runtime.InteropServices;

    /// <summary>
    /// resolve 2gb limit per object .net limitation
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
	class MultiDictionaryCache<TKey, TValue> : ICache<TKey, TValue>
	{
        int _chunkCapacity;
        int _chunksLimit;

        List<Dictionary<TKey, TValue>> _cache;
        //Dictionary<TKey, TValue> _lastChunk;

        public MultiDictionaryCache(int capacity, int memoryLimitMb, int chunks = 100)
		{
            if (capacity <= 0 || memoryLimitMb <= 0 || chunks <= 0) throw new ArgumentException();

            _chunksLimit = chunks;

            var elementSize = Marshal.SizeOf(typeof(TKey)) + Marshal.SizeOf(typeof(TValue)) + sizeof(int) * 3;

            var maxCapacity = capacity;
            var maxMemoryCapacity = memoryLimitMb * 1024 * 1024 / elementSize;
            var maxDictionaryCapacity = int.MaxValue / elementSize;
            maxDictionaryCapacity = DictionarySizeEstimator.FindMaxDictionarySize<TKey, TValue>(Math.Min(maxDictionaryCapacity, maxMemoryCapacity) / 2, Math.Min(maxDictionaryCapacity, maxMemoryCapacity), 1000000);
            _chunkCapacity = Math.Min(maxDictionaryCapacity, Math.Min(maxMemoryCapacity, maxCapacity) / _chunksLimit);

            _cache = new List<Dictionary<TKey, TValue>>(_chunksLimit);
            AddChunk();
#if DEBUG
            Console.WriteLine("debug: MultiDictionaryCache initialized Chunks: {0} ChunkCapacity: {1}  MemoryEstimate: {2}Mb", _chunksLimit, _chunkCapacity, _chunksLimit * _chunkCapacity * elementSize / (1024 * 1024) );
#endif
		}

        public TValue this[TKey key]
        {
            get 
            {
                TValue value = default(TValue);

                foreach (var chunk in _cache)
                {
                    if(chunk.TryGetValue(key, out value)) break;
                }

                return value;
            }
            set
            {
                if (_cache[_cache.Count - 1].Count >= _chunkCapacity) AddChunk();

                if (_cache[_cache.Count - 1].Count < _chunkCapacity)
                {
                    _cache[_cache.Count - 1][key] = value;
                }
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            bool result = false;
            value = default(TValue);

            foreach (var chunk in _cache)
            {
                result = chunk.TryGetValue(key, out value);
                if (result) break;
            }

            return result;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return new Enumerator(_cache);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(_cache);
        }

        private void AddChunk()
        {
            if (_cache.Count < _chunksLimit)
            {
                _cache.Add(new Dictionary<TKey, TValue>(_chunkCapacity));
            }
        }

        class Enumerator : IEnumerator<KeyValuePair<TKey, TValue>>
        {
            List<Dictionary<TKey, TValue>> _cache;
            int _chunk;
            IEnumerator<KeyValuePair<TKey, TValue>> _enumerator;

            public Enumerator (List<Dictionary<TKey, TValue>> cache)
	        {
                _cache = cache;
                Reset();
	        }

            public KeyValuePair<TKey, TValue> Current
            {
                get 
                { 
                    return _enumerator != null ?  _enumerator.Current : default(KeyValuePair<TKey, TValue>); 
                }
            }

            public bool MoveNext()
            {
                if(_chunk < _cache.Count)
                {
                    if(_enumerator == null || !_enumerator.MoveNext())
                    {
                        _chunk++;
                        _enumerator = _chunk < _cache.Count ? _cache[_chunk].GetEnumerator() : (IEnumerator<KeyValuePair<TKey, TValue>>)null;

                        return MoveNext();
                    }

                    return true;
                }

                return false;
            }

            public void Reset()
            {
                _chunk = -1;
                _enumerator = null;
            }

            object IEnumerator.Current { get { return Current; } }

            void IDisposable.Dispose() { }
        }

        public int Size
        {
            get
            {
                var size = _chunksLimit * _chunkCapacity;

                return size;
            }
        }
    }
}
