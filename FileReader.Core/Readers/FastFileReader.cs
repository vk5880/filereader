﻿using System.Linq;
using FileReader.Core.Configuration;

namespace FileReader.Core.Readers
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Text;
	
	public class FastFileReader : IExtendedFileReader
	{
		string _fileName;
		Encoding _encoding;
		protected IConfiguration _configuration;
		protected byte[] _separator;
		protected FileStream _fileStream;
		char[] _lineBuffer;
		byte[] _whitespaces;

        protected int _separatorIndex;
		
		public FastFileReader(string fileName, Encoding encoding, IConfiguration configuration)
		{
			_fileName = fileName;
			_encoding = encoding;
			_configuration = configuration;
			
			_separator = encoding.GetBytes("\r\n");
			//SPACE (U+0020)
			//CHARACTER TABULATION (U+0009), LINE FEED (U+000A), LINE TABULATION (U+000B), FORM FEED (U+000C), CARRIAGE RETURN (U+000D), NEXT LINE (U+0085), and NO-BREAK SPACE (U+00A0).
			_whitespaces = new byte[] {0x20, 9, 10, 11, 12, 13, 0x85, 0xA0};
			
			_fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, _configuration.ReadBufferSize, true);
			_lineBuffer = new char[_configuration.ResultMaxLength];
		}
		
		public virtual string GetLine(long lineIndex)
		{
			if(_fileStream == null) throw new ObjectDisposedException(typeof(FastFileReader).Name);
			
			Rewind();
			var line = ReadLine(lineIndex);
			
			return line;
		}
		
		protected virtual string ReadLine(long lineIndex)
		{
			byte[] buffer;
			int bufferPosition;
			int bufferLength;
            _separatorIndex = 0;
			var currentLineIndex = SeekLineAsync(lineIndex, out buffer, out bufferPosition, out bufferLength);
			
			var line = currentLineIndex == lineIndex ? ReadLine(buffer, bufferPosition, bufferLength) : null;
			
			return line;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="lineIndex"></param>
		/// <returns>lineIndex</returns>
		protected virtual long SeekLineAsync(long lineIndex, out byte[] buffer, out int bufferPosition, out int bufferLength)
		{
            long currentLineIndex = 0;

            var readBuffer = new byte[_configuration.ReadBufferSize];
            buffer = new byte[_configuration.ReadBufferSize];
			bufferPosition = 0;
            bufferLength = 0;

			while(currentLineIndex < lineIndex)
			{
				var readResult = _fileStream.BeginRead(readBuffer, 0, readBuffer.Length, null, null);
				// seek buffer
                currentLineIndex = SeekLine(lineIndex, currentLineIndex, buffer, ref bufferPosition, bufferLength);
				
				var readBufferLength = _fileStream.EndRead(readResult);
				// eof or line found
				if(readBufferLength == 0 || currentLineIndex == lineIndex)
					break;
				
				// exchange buffers
				var bufferExchange = buffer;
				buffer = readBuffer;
				readBuffer = bufferExchange;
                bufferPosition = 0;
				bufferLength = readBufferLength;
			}
			
			return currentLineIndex;
		}
		
		/// <summary>
		/// read buffer to position where line with lineIndex starts
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="lineIndex"></param>
		/// <returns>currentLineIndex</returns>
        protected virtual long SeekLine(long lineIndex, long currentLineIndex, byte[] buffer, ref int bufferPosition, int bufferLength)
		{
			while(currentLineIndex < lineIndex && bufferPosition < bufferLength)
			{
				bufferPosition = SeekSeparator(buffer, bufferPosition, bufferLength);
				// found separator
				if(bufferPosition < bufferLength)
				{
                    //skip separator
					bufferPosition += _separator.Length;
                    // separator is not last character in buffer or _fileStream
					if(bufferPosition < bufferLength || _fileStream.Position < _fileStream.Length)
						currentLineIndex++;
					if(currentLineIndex == lineIndex)
					{
						break;
					}
				}
			}
			
			return currentLineIndex;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="separator"></param>
		/// <param name="buffer"></param>
		/// <param name="bufferPosition"></param>
		/// <param name="bufferLength"></param>
		/// <returns>buffer position</returns>
		protected virtual int SeekSeparator(byte[] buffer, int bufferPosition, int bufferLength)
		{
			while(bufferPosition < bufferLength)
			{
				if(buffer[bufferPosition] == _separator[_separatorIndex])
				{
					if(_separatorIndex == _separator.Length - 1)
					{
						bufferPosition -= _separatorIndex;
						return bufferPosition;
					}
					_separatorIndex++;
				}
				else
					_separatorIndex = 0;
				
				bufferPosition++;
			}

			return bufferPosition;
		}
		
		protected string ReadLine(byte[] buffer, int bufferPosition, int bufferLength)
		{
			var lineBufferPosition = 0;
			bool foundLineEnd = false;
			do
			{
				// fill buffer if it is empty
				if(bufferPosition == bufferLength)
				{
					bufferPosition = 0;
					var maxBytesToRead = _encoding.GetMaxByteCount(_lineBuffer.Length - lineBufferPosition);
					var bytesToRead = maxBytesToRead < buffer.Length ? maxBytesToRead : buffer.Length;
					bufferLength = _fileStream.Read(buffer, bufferPosition, bytesToRead);
					// eof
					if(bufferLength == 0) break;
				}

                // fill lineBuffer with result
				// looking for line end in the buffer
				var separatorPosition = SeekSeparator(buffer, bufferPosition, bufferLength);
				foundLineEnd = separatorPosition < bufferLength;
                var bufferBytesLeft = (foundLineEnd ? separatorPosition : bufferLength) - bufferPosition;
				var resultCharsLeft = _lineBuffer.Length - lineBufferPosition;
                // max chars decoding because of can't predict number of bytes that will be decoded
                var bytesToDecode = Min(bufferBytesLeft, _encoding.GetMaxByteCount(resultCharsLeft));
                var decodedChars = _encoding.GetChars(buffer, bufferPosition, bytesToDecode);
				var charsToCopy = Min(resultCharsLeft, decodedChars.Length);
				for(int i = 0; i < charsToCopy; i++)
				{
					_lineBuffer[lineBufferPosition++] = decodedChars[i];
				}
				
				// read again
				if(bufferLength > 0) bufferLength = 0;
			}
			// exit when foundLineEnd or filled lineBuffer or reached eof
			while(!foundLineEnd && lineBufferPosition < _lineBuffer.Length);
			
			var line = lineBufferPosition == 0 ? string.Empty : new String(_lineBuffer, 0, lineBufferPosition);
			
			return line;
		}
		
		public virtual IDictionary<long, string> GetWords(long lineIndex)
		{
			return GetWords(lineIndex, null);
		}
		
		public virtual IDictionary<long, string> GetWords(long lineIndex, long[] wordsIndexes)
		{
			if(_fileStream == null) throw new ObjectDisposedException(typeof(FastFileReader).Name);
			
			Rewind();
			var words = ReadWords(lineIndex, wordsIndexes);
			
			return words;
		}
		
		protected virtual IDictionary<long, string> ReadWords(long lineIndex, long[] wordsIndexes)
		{
			byte[] buffer;
			int bufferPosition;
			int bufferLength;
			var currentLineIndex = SeekLineAsync(lineIndex, out buffer, out bufferPosition, out bufferLength);
			
			var words = currentLineIndex == lineIndex ? ReadWords(wordsIndexes, buffer, bufferPosition, bufferLength) : null;
			
			return words;
		}
		
		protected virtual IDictionary<long, string> ReadWords(long[] wordsIndexes, byte[] buffer, int bufferPosition, int bufferLength)
		{
			var words = new Dictionary<long, string>();
			
			var maxWordIndex = wordsIndexes != null ? wordsIndexes.Max() : long.MaxValue;
			for(var wordIndex = 0; wordIndex <= maxWordIndex; wordIndex++)
			{
				SkipWhitespaces(buffer, ref bufferPosition, ref bufferLength);
				
				if(wordsIndexes == null || wordsIndexes.Contains(wordIndex))
				{
					bool foundSeparator;
					var word = ReadWord(buffer, ref bufferPosition, ref bufferLength, out foundSeparator);
					if(word == null) break;
					
					words.Add(wordIndex, word);
					if(foundSeparator) break;
				}
				else
				{
					var hasNextWord = SkipWord(buffer, ref bufferPosition, ref bufferLength);
					if(!hasNextWord) break;
				}
			}
			
			return words;
		}
		
		protected virtual bool SkipWord(byte[] buffer, ref int bufferPosition, ref int bufferLength)
		{
			bool hasSeparator = false;
			bool hasWhitespace = false;
			
			do
			{
				FillBuffer(buffer, ref bufferPosition, ref bufferLength);
				if(bufferPosition == bufferLength) break;
				
				var separatorPosition = SeekSeparator(buffer, bufferPosition, bufferLength);
				hasSeparator = separatorPosition < bufferLength;
				var whitespacePosition = SeekWhitespace(buffer, bufferPosition, bufferLength);
				hasWhitespace = whitespacePosition < bufferLength;
				if(hasSeparator && hasWhitespace && separatorPosition <= whitespacePosition)
				{
					hasWhitespace = false;
				}
				
				bufferPosition = hasSeparator || hasWhitespace ? Min(separatorPosition, whitespacePosition) : bufferLength;
			}
			while(!hasSeparator && !hasWhitespace);
				
			if(hasWhitespace) 
			{
				return true;
			}
			
			return false;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="bufferPosition"></param>
		/// <param name="bufferLength"></param>
		/// <returns>bufferPosition</returns>
		protected virtual int SeekWhitespace(byte[] buffer, int bufferPosition, int bufferLength)
		{
			while(bufferPosition < bufferLength)
			{
				for(var j = 0; j < _whitespaces.Length; j++)
				{
					if(buffer[bufferPosition] == _whitespaces[j])
					{
						return bufferPosition;
					}
				}
				
				bufferPosition++;
			}
			
			return bufferPosition;
		}
		
		protected virtual void SkipWhitespaces(byte[] buffer, ref int bufferPosition, ref int bufferLength)
		{
			FillBuffer(buffer, ref bufferPosition, ref bufferLength);
			
			// skipping leading whitespaces
			while(bufferPosition < bufferLength)
			{
				bool whitespace = false;
				for(var j = 0; j < _whitespaces.Length; j++)
				{
					if(buffer[bufferPosition] == _whitespaces[j])
					{
						whitespace = true;
						break;
					}
				}
				
				bool separator = true;
				for(int k = 0; k < _separator.Length; k++)
				{
					if(bufferPosition + k >= bufferLength || buffer[bufferPosition + k] != _separator[k])
					{
						separator = false;
						break;
					}
				}
				
				if(!whitespace || separator) break;
				bufferPosition++;
				
				FillBuffer(buffer, ref bufferPosition, ref bufferLength);
			}
		}
		
		void FillBuffer(byte[] buffer, ref int bufferPosition, ref int bufferLength)
		{
			if(bufferPosition == bufferLength)
			{
				bufferPosition = 0;
				bufferLength = _fileStream.Read(buffer, bufferPosition, buffer.Length);
			}
		}
		
		protected virtual string ReadWord(byte[] buffer, ref int bufferPosition, ref int bufferLength, out bool foundEOL)
		{
			var lineBufferPosition = 0;
			bool foundWhitespace = false;
			bool foundSeparator = false;
			foundEOL = false;
			do
			{
				FillBuffer(buffer, ref bufferPosition, ref bufferLength);
				if(bufferPosition == bufferLength) break;
				
				var separatorPosition = SeekSeparator(buffer, bufferPosition, bufferLength);
				foundSeparator = separatorPosition < bufferLength;
				var whitespacePosition = SeekWhitespace(buffer, bufferPosition, bufferLength);
				foundWhitespace = whitespacePosition < bufferLength;
				var bufferBytesLeft = Min(foundSeparator ? separatorPosition : bufferLength,
				                          foundWhitespace ? whitespacePosition : bufferLength) - bufferPosition;
				var bufferCharsLeft =  _encoding.GetCharCount(buffer, bufferPosition, bufferBytesLeft);
				var lineBufferCharsLeft = _lineBuffer.Length - lineBufferPosition;
				var charsToCopy = Min(lineBufferCharsLeft, bufferCharsLeft);
				var bytesToCopy = Min(_encoding.GetMaxByteCount(lineBufferCharsLeft), bufferBytesLeft);
				var chars = _encoding.GetChars(buffer, bufferPosition, bytesToCopy);
				for(int i = 0; i < charsToCopy; i++)
				{
					_lineBuffer[lineBufferPosition++] = chars[i];
				}
				
				//if(separatorPosition == whitespacePosition) foundWhitespace = false;
				if(foundSeparator && separatorPosition <= whitespacePosition) foundEOL = true;
				
				bufferPosition = Min(foundSeparator ? separatorPosition: bufferLength, foundWhitespace ? whitespacePosition: bufferLength);
			}
			while(!foundSeparator && !foundWhitespace && lineBufferPosition < _lineBuffer.Length);
			
			if(!foundSeparator && !foundWhitespace) SkipWord(buffer, ref bufferPosition, ref bufferLength);
			
			var line = lineBufferPosition > 0 ? new String(_lineBuffer, 0, lineBufferPosition) : null;
			
			return line;
		}
		
		private int Min(int a, int b)
		{
			return a < b ? a : b;
		}
		
		private int Min(int a, int b, int c)
		{
			return a < b ? (a < c ? a : c) : (b < c ? b : c);
		}
		
		public void Dispose()
		{
			if(IsStreamValid())
			{
				_fileStream.Dispose();
				_fileStream = null;
			}
		}
		
		protected void Rewind()
		{
			_fileStream.Seek(0, SeekOrigin.Begin);
		}
		
		private bool IsStreamValid()
		{
			return _fileStream != null;
		}
	}
}
