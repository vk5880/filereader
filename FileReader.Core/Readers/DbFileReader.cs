﻿using System.Linq;
using FileReader.Core.Data;
using FileReader.Core.Data.Entities;

namespace FileReader.Core.Readers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FileReader.Core.Configuration;

    public class DbFileReader : IFileReader
    {
        FileReaderContext _db;
        IExtendedFileReader _reader;

        public DbFileReader(string fileName, Encoding encoding, IConfiguration configuration)
        {
        	_reader = new IndexingFileReader(fileName, encoding, configuration);
            _db = new FileReaderContext();
            
            InitializeDB();
            CacheToDatabase();
        }

        public string GetLine(long lineIndex)
        {
            var line = _db.Lines.Where(l => l.Id == lineIndex).SingleOrDefault();
            var result = line != null ? line.Value : null;
            
            return result;
        }

        public IDictionary<long, string> GetWords(long lineIndex, long[] wordsIndexes)
        {
        	throw new StackOverflowException();
        	//var words = _db.Words.Where(w => w.LineId == lineIndex && wordsIndexes.Contains(w.Id)).ToDictionary(e => (long)e.Id, e => e.Value);

            //return words;
        }

        public void Dispose()
        {
            if (_reader != null)
            {
                _reader.Dispose();
                _reader = null;
            }
            if (_db != null)
            {
                _db.Dispose();
                _db = null;
            }
        }

        /// <summary>
        /// Clean up Lines and Words db tables from previous file
        /// </summary>
        private void InitializeDB()
        {
        	//_db.Database.ExecuteSqlCommand(string.Format("TRUNCATE TABLE \"{0}s\" CASCADE", typeof(Line).Name));
        	//_db.Database.ExecuteSqlCommand(string.Format("DELETE FROM \"{0}s\" ", typeof(Line).Name));
            
            //_db.SaveChanges();
        }

        /// <summary>
        /// Read file and add all lines and all words limited to ResultMaxLength to db
        /// </summary>
        private void CacheToDatabase()
        {
            int lineIndex = 0;

            string lineValue;
            while ((lineValue = _reader.GetLine(lineIndex)) != null)
            {
            	var line = new Line { Id = lineIndex, Value = lineValue };
                _db.Lines.Add(line);
                _db.SaveChanges();
                
                var words = _reader.GetWords(lineIndex);
                foreach(var readWord in words)
                {
                	var word = new Word { Id = readWord.Key, Value = readWord.Value };
                    _db.Words.Add(word);
                }

                _db.SaveChanges();
                lineIndex++;
            }
        }
    }
}
