﻿namespace FileReader.Core.Readers
{
	using System;
	using System.Text;
	using FileReader.Core.Configuration;
	
	public class CachingFileReader : SimpleFileReader
	{
		public CachingFileReader(string fileName, Encoding encoding, IConfiguration configuration)
			: base(fileName, encoding, configuration)
		{
			if(!Cache.Initialized) {
				var cacheMemoryLimitInMb = configuration.RAMLimitMb / 2;
				Cache.Initialize(cacheMemoryLimitInMb);
			}
		}
		
		protected override string ReadLine(long lineIndex)
		{
			if (lineIndex < 0 || lineIndex > lastLineIndex) return null;

			CheckReaderAviability();
			RewindReader();
			
			long n = 0;
			string line = null;
			while ((line = _reader.ReadLine()) != null && n < lineIndex)
			{
				if(line.Length > _configuration.ResultMaxLength)
					line = line.Substring(0, _configuration.ResultMaxLength);
                Cache.Instance.CacheLine(n, line);
				n++;
			}
			if (line == null)
			{
				lastLineIndex = n;
			}

            Console.WriteLine(Cache.Instance.CacheSize());
			return line;
		}
		
		public override void Dispose()
		{
			Cache.Free();
			base.Dispose();
		}
	}
}
