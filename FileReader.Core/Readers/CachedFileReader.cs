﻿namespace FileReader.Core.Readers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FileReader.Core.Configuration;

    public class CachedFileReader : IFileReader
    {
        IFileReader _reader;

        public CachedFileReader(IFileReader reader, IConfiguration configuration)
        {
            _reader = reader;
            if(!MemoryCache.Initialized) 
            {
            	var cacheMemoryLimitInMb = configuration.RAMLimitMb / 2;
            	MemoryCache.Initialize(cacheMemoryLimitInMb);
            }
        }

        public string GetLine(long lineIndex)
        {
        	string line = MemoryCache.Instance.GetLine(lineIndex);
            if (line == null)
            {
                line = _reader.GetLine(lineIndex);
                MemoryCache.Instance.CacheLine(lineIndex, line);
            }

            return line;
        }

        public IDictionary<long, string> GetWords(long lineIndex, long[] wordsIndexes)
        {
            var words = wordsIndexes
                .Select(n => new {index = n, word = MemoryCache.Instance.GetWord(lineIndex, n)})
                .ToDictionary(e => e.index, e => e.word);
            
            var missedCacheWords = words.Where(e => e.Value == null);
            if(missedCacheWords.Any())
            {
                var missedCacheWordsIndexes = missedCacheWords.Select(e => e.Key).ToArray();
                var readWords = _reader.GetWords(lineIndex, missedCacheWordsIndexes);
                if (readWords != null)
                {
                    foreach (var readWord in readWords)
                    {
                        MemoryCache.Instance.CacheWord(lineIndex, readWord.Key, readWord.Value);
                        words[readWord.Key] = readWord.Value;
                    }
                }
            }

            return words;
        }

        public void Dispose()
        {
            MemoryCache.Free();
            _reader.Dispose();
        }
    }
}
