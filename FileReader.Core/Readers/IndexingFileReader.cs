﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileReader.Core.Configuration;
using System.IO;
using FileReader.Core.Caches;

namespace FileReader.Core.Readers
{
    public class IndexingFileReader : FastFileReader
    {
        ICache<long, long> _index;
        long _indexStep;
        long _lastIndex;

        public IndexingFileReader(string fileName, Encoding encoding, IConfiguration configuration)
            : base(fileName, encoding, configuration)
        {
            var maxFileLines = _fileStream.Length / _separator.Length;
            var maxDictionaryLines = maxFileLines < int.MaxValue ? (int)maxFileLines : int.MaxValue;
            _index = new MultiDictionaryCache<long, long>(maxDictionaryLines, configuration.RAMLimitMb);

            UpdateIndexStep(maxDictionaryLines);
            _lastIndex = 0;
        }

        public override string GetLine(long lineIndex)
        {
			if(_fileStream == null) throw new ObjectDisposedException("_fileStream");

            long currentLineIndex = SeekClosestLine(lineIndex);
            var line = ReadLine(lineIndex, currentLineIndex);

            return line;
        }

        protected long SeekClosestLine(long lineIndex)
        {
            long closestLineIndex = 0;
            long position;
#if DEBUG
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
#endif
            if (_index.TryGetValue(lineIndex, out position))
            {
                closestLineIndex = lineIndex;
            }
            else
            {
                //closestLineIndex = _index.Where(e => e.Key < lineIndex).Max(e => e.Key);
                foreach (var e in _index)
                {
                    if (e.Key < lineIndex && e.Key > closestLineIndex)
                    {
                        closestLineIndex = e.Key;
                    }
                }
                position = _index[closestLineIndex];
            }
#if DEBUG
            sw.Stop();
            Console.WriteLine("debug: index lookup {0} ms", sw.ElapsedMilliseconds);
#endif
            Seek(position);

            return closestLineIndex;
        }

        protected virtual string ReadLine(long lineIndex, long currentLineIndex)
        {
            byte[] buffer;
            int bufferPosition;
            int bufferLength;
            _separatorIndex = 0;
            currentLineIndex = SeekLineAsync(lineIndex, currentLineIndex, out buffer, out bufferPosition, out bufferLength);

            var line = currentLineIndex == lineIndex ? ReadLine(buffer, bufferPosition, bufferLength) : null;
            if (line == null)
            {
                //eof and currentLineIndex is last
                UpdateIndexStep(currentLineIndex);
            }

            return line;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lineIndex"></param>
        /// <returns>lineIndex</returns>
        protected long SeekLineAsync(long lineIndex, long currentLineIndex, out byte[] buffer, out int bufferPosition, out int bufferLength)
        {
            var readBuffer = new byte[_configuration.ReadBufferSize];
            buffer = new byte[_configuration.ReadBufferSize];
            bufferPosition = 0;
            bufferLength = 0;
            long bufferStreamPositon = _fileStream.Position;

            while (currentLineIndex < lineIndex)
            {
                var readResult = _fileStream.BeginRead(readBuffer, 0, readBuffer.Length, null, null);
                // seek buffer
                currentLineIndex = SeekLine(lineIndex, currentLineIndex, buffer, ref bufferPosition, bufferLength, bufferStreamPositon);

                var readBufferLength = _fileStream.EndRead(readResult);
                // eof or line found
                if (readBufferLength == 0 || currentLineIndex == lineIndex)
                    break;

                // exchange buffers
                var bufferExchange = buffer;
                buffer = readBuffer;
                readBuffer = bufferExchange;
                bufferPosition = 0;
                bufferLength = readBufferLength;
                bufferStreamPositon = _fileStream.Position;
            }

            return currentLineIndex;
        }

        protected long SeekLine(long lineIndex, long currentLineIndex, byte[] buffer, ref int bufferPosition, int bufferLength, long bufferStreamPosition)
        {
            while (currentLineIndex < lineIndex && bufferPosition < bufferLength)
			{
				bufferPosition = SeekSeparator(buffer, bufferPosition, bufferLength);
				// found separator
				if(bufferPosition < bufferLength)
				{
					bufferPosition += _separator.Length;
                    // separator is not last character in buffer or _fileStream
                    if (bufferPosition < bufferLength || _fileStream.Position < _fileStream.Length)
                    {
                        currentLineIndex++;
                        
                        IndexLine(currentLineIndex, bufferStreamPosition - bufferLength + bufferPosition);
                    }
					if(currentLineIndex == lineIndex)
					{
						break;
					}
				}
			}
			
			return currentLineIndex;
        }

        public override IDictionary<long, string> GetWords(long lineIndex)
        {
        	return GetWords(lineIndex, null);
        }
        		
        public override IDictionary<long, string> GetWords(long lineIndex, long[] wordsIndexes)
        {
            if (_fileStream == null) throw new ObjectDisposedException("_fileStream");

            long currentLineIndex = SeekClosestLine(lineIndex);
            var words = ReadWords(lineIndex, currentLineIndex, wordsIndexes);

            return words;
        }

        protected virtual IDictionary<long, string> ReadWords(long lineIndex, long currentLineIndex, long[] wordsIndexes)
        {
            byte[] buffer;
            int bufferPosition;
            int bufferLength;
            currentLineIndex = SeekLineAsync(lineIndex, currentLineIndex, out buffer, out bufferPosition, out bufferLength);

            var words = currentLineIndex == lineIndex ? ReadWords(wordsIndexes, buffer, bufferPosition, bufferLength) : null;
            if (currentLineIndex != lineIndex)
            {
                //eof and currentLineIndex is last
                UpdateIndexStep(currentLineIndex);
            }

            return words;
        }

        protected void Seek(long position)
        {
            if (position < 0 || position >= _fileStream.Length) throw new ArgumentException("invalid seeking closestLineIndex");
            var offset = position - _fileStream.Position;
            _fileStream.Seek(offset, SeekOrigin.Current);
        }

        protected void UpdateIndexStep(long maxSize)
        {
            _indexStep = maxSize / _index.Size > 1 ? maxSize / _index.Size : 1;
        }

        protected void IndexLine(long lineIndex, long position)
        {
            if (lineIndex % _indexStep == 0)
            {
                _index[lineIndex] = position;
                _lastIndex = lineIndex;
            }
        }
    }
}
