﻿namespace FileReader.Core.Readers
{
	using System.Collections.Generic;
	
	public interface IExtendedFileReader : IFileReader
	{
        IDictionary<long, string> GetWords(long lineIndex);
	}
}
