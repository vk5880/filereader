﻿namespace FileReader.Core.Readers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using FileReader.Core.Configuration;
    using System.Collections.Generic;

    public class SimpleFileReader : IExtendedFileReader
    {
        protected StreamReader _reader;
        protected IConfiguration _configuration;
        protected long lastLineIndex;

        public SimpleFileReader(string fileName, Encoding encoding, IConfiguration configuration)
        {
            _configuration = configuration;

            //The buffer size, in number of 16-bit characters
            var bufferSizeInCharacters = _configuration.ReadBufferSize / 16;

            var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, _configuration.ReadBufferSize); 
            _reader = new StreamReader(fileName, encoding, false, bufferSizeInCharacters);
            lastLineIndex = long.MaxValue;
        }

        public virtual string GetLine(long lineIndex)
        {
            string line = ReadLine(lineIndex);

            if (line != null && line.Length > _configuration.ResultMaxLength)
            {
                line = line.Substring(0, _configuration.ResultMaxLength);
            }

            return line;
        }
        
        public virtual IDictionary<long, string> GetWords(long lineIndex, long[] wordsIndexes)
        {
            var line = ReadLine(lineIndex);
            var words = GetWords(line, wordsIndexes);

            return words;
        }

        IDictionary<long, string> IExtendedFileReader.GetWords(long lineIndex)
        {
        	return GetWords(lineIndex, null);
        }
        
        public virtual void Dispose()
        {
            if (_reader != null)
            {
                _reader.Dispose();
                _reader = null;
            }
        }

        protected virtual string ReadLine(long lineIndex)
        {
            if (lineIndex < 0 || lineIndex > lastLineIndex) return null;

            CheckReaderAviability();
            RewindReader();

            long n = 0;
            string line = null;
            while ((line = _reader.ReadLine()) != null && n < lineIndex) 
            {
                n++;
            }
            if (line == null)
            {
                lastLineIndex = n;
            }

            return line;
        }

        protected virtual void RewindReader()
        {
            _reader.BaseStream.Seek(0, SeekOrigin.Begin);
            _reader.DiscardBufferedData();
        }
        
        protected virtual IDictionary<long, string> GetWords(string line, long[] wordsIndexes = null)
        {
            if (line == null) return null;

            long wordIndex = 0;
            var words = Regex
            	.Split(line.Trim(), @"\s+")
            	.ToDictionary(e => wordIndex++, e => e.Substring(0, _configuration.ResultMaxLength));
            if(wordsIndexes != null)
            {
            	words = words.Where(e => wordsIndexes.Contains(e.Key)).ToDictionary(e => e.Key, e => e.Value);
            }
            
            return words;
        }

        protected void CheckReaderAviability()
        {
            if (_reader == null) throw new ObjectDisposedException("file");
        }
    }
}
