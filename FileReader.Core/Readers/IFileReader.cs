﻿namespace FileReader.Core.Readers
{
    using System;
    using System.Collections.Generic;

    public interface IFileReader : IDisposable
    {
        string GetLine(long lineIndex);
        IDictionary<long, string> GetWords(long lineIndex, long[] wordsIndexes);
    }
}
