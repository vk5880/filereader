﻿namespace FileReader.Core.Data.Entities
{
	/// <summary>
	/// Description of Word.
	/// </summary>
	public class Word
	{
		public long Id { get; set; }
		public long LineId { get; set; }
		public string Value { get; set; }
	}
}
