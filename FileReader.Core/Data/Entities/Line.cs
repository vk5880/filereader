﻿namespace FileReader.Core.Data.Entities
{
	public class Line
	{
		public long Id { get; set; }
		public string Value { get; set; }
	}
}
