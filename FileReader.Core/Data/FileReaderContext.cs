﻿namespace FileReader.Core.Data
{
	using System.Data.Entity;
	using FileReader.Core.Data.Entities;
	
	public class FileReaderContext : DbContext
	{
		public DbSet<Line> Lines { get; set; }
		public DbSet<Word> Words { get; set; }
		
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Line>().ToTable("public.Lines");
			modelBuilder.Entity<Word>().ToTable("public.Words");
		}
	}
}
