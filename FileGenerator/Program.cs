﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace FileGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length >= 2)
            {
                string fileName;
                int linesCount = 0;
                int maxWords = 0;
                int.TryParse(args[0], out linesCount);
                int.TryParse(args[1], out maxWords);

                if (args.Length == 2) 
                    fileName = string.Format(".\\{0}_{1}.txt", linesCount, maxWords);
                else 
                    fileName = args[2];

                GenerateFile(fileName, linesCount, maxWords);

                Console.WriteLine("Generation completed");

                return;
            }

            Console.WriteLine("usage:FileGenerator <linesCount> <maxWords> [<fileName>]");
        }

        private static void GenerateFile(string fileName, int linesCount, int maxWords, string wordFormat = "{0}:{1}")
        {
            using(var streamWriter = new StreamWriter(fileName, false, Encoding.ASCII))
            {
                var random = new Random();
                for(int i = 0; i < linesCount; i++)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat(wordFormat, i, 0);

                    var wordsCount = random.Next(maxWords + 1);
                    for(var j = 1; j < wordsCount; j++)
                    {
                        sb.AppendFormat(" " + wordFormat, i, j);
                    }

                    var line = sb.ToString();
                    streamWriter.WriteLine(line);
                }
            }
        }
    }
}
