﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileReader.Commands
{
    interface ICommand
    {
        string Execute();
    }
}
