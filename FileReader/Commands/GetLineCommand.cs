﻿namespace FileReader.Commands
{
    using FileReader.Core.Readers;

    class GetLineCommand : ICommand
    {
        IFileReader _reader;
        long _lineIndex;

        public GetLineCommand(IFileReader reader, long lineIndex)
        {
            _reader = reader;
            _lineIndex = lineIndex;
        }

        public string Execute()
        {
            var result = _reader.GetLine(_lineIndex);

            return result ?? string.Format("error: line with index {0} has not been found", _lineIndex);
        }
    }
}
