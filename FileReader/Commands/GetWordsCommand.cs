﻿namespace FileReader.Commands
{
    using System.Text;
    using System.Linq;
    using FileReader.Core.Readers;
    using FileReader.Core.Configuration;

    class GetWordsCommand : ICommand
    {
        IFileReader _reader;
        long _lineIndex;
        long[] _wordsIndexes;

        public GetWordsCommand(IFileReader reader, long lineIndex, long[] wordsIndexes)
        {
            _reader = reader;
            _lineIndex = lineIndex;
            _wordsIndexes = wordsIndexes;
        }

        public string Execute()
        {
            var words = _reader.GetWords(_lineIndex, _wordsIndexes);

            string result;
            if (words == null)
            {
                result = string.Format("error: line with closestLineIndex {0} has not been found", _lineIndex);
            }
            else if (words.Count == 0)
            {
                result = string.Format("error: no words with [{0}] indexes have been found", string.Join(",", _wordsIndexes));
            }
            else
            {
                var foundWords = words.Values.Where(e => e != null).ToArray();
                var foundWordsCount = foundWords.Length;
                result = string.Join(" ", foundWords);
                if (foundWordsCount < _wordsIndexes.Length)
                {
                    result += string.Format("\nwarning: only {0} word{1} ha{2} been found", foundWordsCount, foundWordsCount == 1 ? string.Empty : "s", foundWordsCount == 1 ? "s" : "ve");
                }
            }

            return result;
        }
    }
}
