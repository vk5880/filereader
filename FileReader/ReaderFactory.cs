﻿namespace FileReader
{
    using System;
    using System.Text;
    using FileReader.Core.Readers;
    using FileReader.Core.Configuration;
    using System.Reflection;

    static class ReaderFactory
    {
        public static IFileReader Build(string fileName, Encoding encoding, IConfiguration configuration)
        {
            IFileReader reader;

            if (!string.IsNullOrWhiteSpace(configuration.ReaderName))
            {
                var handle = Activator.CreateInstance(
                    "FileReader.Core", 
                    "FileReader.Core.Readers." + configuration.ReaderName, 
                    false, 
                    BindingFlags.Default, 
                    null, 
                    new object[] { fileName, encoding, configuration }, 
                    null, 
                    null);

                reader = handle.Unwrap() as IFileReader;
            }
            else
            {
                reader = new DbFileReader(fileName, encoding, configuration);
            }

            if (configuration.ResultCaching)
            {
                reader = new CachedFileReader(reader, configuration);
            }

            return reader;
        }
    }
}
