﻿namespace FileReader
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using Commands;
    using CommandConstructors;
    using Core.Readers;

    class CommandFactory
    {
        IFileReader _reader;
        IEnumerable<ICommandConstructor> _constructors;
        
        public CommandFactory(IFileReader reader)
        {
            _reader = reader;
            _constructors = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.GetInterface(typeof(ICommandConstructor).Name) != null)
                .Select(t => Activator.CreateInstance(t) as ICommandConstructor)
                .ToArray();
        }

        public virtual ICommand Build(string command)
        {
            foreach(var constructor in _constructors)
            {
                var match = Regex.Match(command, constructor.Pattern, RegexOptions.IgnoreCase & RegexOptions.ExplicitCapture);
                if (match.Success) 
                {
                    return constructor.Construct(_reader, match);
                }
            }

            return null;
        }
    }
}
