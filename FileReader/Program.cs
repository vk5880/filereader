﻿namespace FileReader
{
    using System;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.IO;
    using System.Linq;
    using Core.Configuration;
    using FileReader.Core.Readers;
    using System.Collections.Generic;

    class Program
    {
        static AssemblyName assemblyName = Assembly.GetExecutingAssembly().GetName();
        static IConfiguration _configuration;

        static void Main(string[] args)
        {
            PrintWelcome();

            var fileName = GetFileName(args);
            if (fileName == null)
            {
                PrintFilenameError();
                PrintGoodbye();
                return; 
            }

            if (!FileIsValid(fileName))
            {
                PrintFileError(fileName);
                PrintGoodbye();
                return;
            }

            _configuration = GetConfiguration(args);

            using (var reader = ReaderFactory.Build(fileName, Encoding.ASCII, _configuration))
            {
                var commandFactory = new CommandFactory(reader);
                ProcessCommands(commandFactory);
            }

            PrintGoodbye();
        }

        private static IConfiguration GetConfiguration(string[] args)
        {
            var configuration = new HardCodedConfiguration();

            for(int i = 0; i < args.Length - 1; i++)
            {
                if (args[i] == "--reader" && ++i < args.Length)
                {
                    configuration.ReaderName = args[i];
                    continue;
                }

                if (args[i] == "--noResultsCaching")
                {
                    configuration.ResultCaching = false;
                    continue;
                }
            }

            return configuration;
        }

        static void PrintWelcome()
        {
            PrintLine("\n{0} Version {1}", assemblyName.Name, assemblyName.Version);
            PrintLine("Now you can get lines and words from ascii text file!");
        }

        static string GetFileName(string[] args)
        {
            if (args.Length == 0) return null;
            var fileName = args[args.Length - 1];

            return fileName;
        }

        static bool FileIsValid(string fileName)
        {
            return File.Exists(fileName);
        }

        private static void ProcessCommands(CommandFactory commands)
        {
            while (true)
            {
                var requestedCommand = RequestCommand();
                //no more commands
                if (requestedCommand == null) break;

                var command = commands.Build(requestedCommand);
                if (command != null)
                {
#if DEBUG
                    var stopwatch = new System.Diagnostics.Stopwatch();
                    stopwatch.Start();
#endif
                    var result = command.Execute();

                    //quit command
                    if (result == null) break;
                    PrintLine(result);
                    
#if DEBUG
                    stopwatch.Stop();
                    PrintLine("info: execution time is {0}", stopwatch.Elapsed);
#endif
                }
                else
                {
                    PringCommandError();
                }
            }
        }

        static string RequestCommand()
        {
            Print("\ncommand:");
            var command = Console.ReadLine();
            if (command != null) command = command.Trim();

            return command;
        }

        private static void PrintGoodbye()
        {
            Print("\nPress any key to exit ...");
            Console.ReadKey();
        }

        static void PrintFilenameError()
        {
            PrintLine("\nerror: wrong number of the arguments");
            PrintLine("usage: {0} [<options>] <fileName>", assemblyName.Name);
            var supportedReadersString = string.Join(", ", GetSupportedReaders().Select(t => t.Name));
            PrintLine("options: --reader <readerName> \tReader implemntation. Supported values: ", supportedReadersString);
        }

        static IEnumerable<Type> GetSupportedReaders()
        {
            return Assembly.GetExecutingAssembly().GetTypes().Where(t => t.GetInterface(typeof(IFileReader).Name) != null);
        }

        static void PrintFileError(string fileName)
        {
            PrintLine("\nerror: file {0} does not exist", fileName);
        }

        static void PringCommandError()
        {
            PrintLine("\nerror: wrong command");
            PrintLine("usage: <command> [<args>]");
            PrintLine("commands:");
            PrintLine("\t get <n>\t\tPrint first {0} characters of n-th line in a file", _configuration.ResultMaxLength);
            PrintLine("\t get <n> <k1>..<k2>\tPrint first {0} characters of k1..k2 words of n-th line in a file", _configuration.ResultMaxLength);
            PrintLine("\t quit\t\t\tQuit from the application");
        }

        static void Print(string value)
        {
            Console.Write(value);
        }

        static void PrintLine(string value)
        {
            Console.WriteLine(value);
        }

        static void PrintLine(string format, params object[] args)
        {
            Console.WriteLine(format, args);
        }
    }
}
