﻿namespace FileReader.CommandConstructors
{
    using System;
    using System.Text.RegularExpressions;
    using FileReader.Commands;
    using FileReader.Core.Configuration;
    using FileReader.Core.Readers;

    class QuitConstructor : ICommandConstructor
    {
        public string Pattern
        {
            get { return @"^\s*quit\s*$"; }
        }

        public ICommand Construct(IFileReader reader, Match match)
        {
            return new QuitCommand();
        }
    }
}
