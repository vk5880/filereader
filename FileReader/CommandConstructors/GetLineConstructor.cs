﻿namespace FileReader.CommandConstructors
{
    using System.Text.RegularExpressions;
    using FileReader.Commands;
    using FileReader.Core.Readers;

    class GetLineConstructor : ICommandConstructor
    {
        public string Pattern
        {
            get { return @"^\s*get\s+(?<lineIndex>\d+)\s*$"; }
        }

        public ICommand Construct(IFileReader reader, Match match)
        {
            long lineIndex;
            if(long.TryParse(match.Groups["lineIndex"].Value, out lineIndex))
            {
                return new GetLineCommand(reader, lineIndex);
            }

            return null;
        }
    }
}
