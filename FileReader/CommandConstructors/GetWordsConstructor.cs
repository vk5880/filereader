﻿namespace FileReader.CommandConstructors
{
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using FileReader.Commands;
    using FileReader.Core.Readers;

    class GetWordsConstructor : ICommandConstructor
    {
        public string Pattern
        {
            get { return @"^\s*get\s+(?<lineIndex>\d+)(?:\s+(?<wordIndex>\d+))+\s*$"; }
        }

        public ICommand Construct(IFileReader reader, Match match)
        {
            long lineIndex;
            if(long.TryParse(match.Groups["lineIndex"].Value, out lineIndex))
            {
                var wordsIndexes = new List<long>();
                foreach (Capture capture in match.Groups["wordIndex"].Captures)
                {
                    long wordIndex;
                    if (long.TryParse(capture.Value, out wordIndex))
                    {
                        wordsIndexes.Add(wordIndex);
                    }
                }

                return new GetWordsCommand(reader, lineIndex, wordsIndexes.ToArray());
            }

            return null;
        }
    }
}
