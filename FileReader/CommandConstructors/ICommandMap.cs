﻿namespace FileReader.CommandConstructors
{
    using System.Text.RegularExpressions;
    using FileReader.Commands;
    using FileReader.Core.Readers;
     
    interface ICommandConstructor
    {
        string Pattern { get; }
        ICommand Construct(IFileReader reader, Match match); 
    }
}
